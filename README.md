# StarNavi - demo project

#### Tasks

[Instructions](python_developer_starnavi_test.pdf)


#### Installation instructions

* Create python environment

* Activate it
* Install dependencies
~~~commandline
pip install -r requirements.txt
~~~

* Run migrations
~~~commandline
python manage.py migrate
~~~

* Install frontend requirements
~~~commandline
cd frontend && npm install 
~~~

* Run django server 
~~~commandline
python manage.py runserver
~~~

* Run VueJS server
~~~commandline
cd frontend && npm run serve
~~~

#### Configuration

In 'commuinity/prod_settings.py' you need to define API keys. (Example in the local_settings)
In 'frontend/src/api/config.js' you need to define endpoints, static and media path 

### BOT

Bot dir: bot/
In the bot's root directory after job finished you can find file 'user.txt' with all user's credentials.

* Run bot
~~~commandline
cd bot && python init_bot.py
~~~

#### BOT configuration


### API documentation

Located by URL http://<server_url>/docs/

P.S. In repo you can find real API keys. It's not mistaken, I do that for more comfortable review of the demo project. 