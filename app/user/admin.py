from django.contrib import admin

from .models import *

# Register your models here.


class CustomUserAdmin(admin.ModelAdmin):
    model = CustomUser

    list_display = ('username', 'email', 'country', 'city', )
    search_fields = ('username', 'email', )

admin.site.register(CustomUser, CustomUserAdmin)
