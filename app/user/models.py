import clearbit
import requests

from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django.contrib.auth.models import AbstractUser

# Create your models here.


class CustomUser(AbstractUser):
    """
    Custom user model
    """

    country = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    lat = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)
    lng = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)
    avatar = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.username

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.email and settings.HUNTER_API != '':
                response = requests.get('https://api.hunter.io/v2/email-verifier?email={}&api_key={}'
                                        .format(self.email, settings.HUNTER_API))

                if response.status_code == 200:
                    result = response.json()
                    if result['data']['score']:
                        if result['data']['score'] < 30:
                            raise ValidationError('Probably email does not exist', params={'email': self.email})
                else:
                    if not response.status_code == 429:
                        # 429 == too_many_requests
                        raise ValidationError('Probably email does not exist', params={'email': self.email})

            if self.email:
                response = None

                try:
                    response = clearbit.Enrichment.find(email=self.email, stream=True)
                except Exception as e:
                    print(e)

                if response:
                    if response['person'] is not None:
                        self.country = response['person']['geo']['country']
                        self.city = response['person']['geo']['city']
                        self.state = response['person']['geo']['state']
                        self.lat = response['person']['geo']['lat']
                        self.lng = response['person']['geo']['lng']
                        self.avatar = response['person']['avatar']
                        self.first_name = response['person']['name']['givenName'] or ''
                        self.last_name = response['person']['name']['familyName'] or ''

        super(CustomUser, self).save(*args, **kwargs)

