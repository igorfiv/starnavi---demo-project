from django.test import TestCase
from django.urls import reverse
from rest_framework.test import RequestsClient

from .models import *


class UserTest(TestCase):

    def setUp(self):

        client = RequestsClient()

        CustomUser.objects.create_user(
            username='testtest',
            password='qwe123456'
        )

    def test_obtain_token(self):
        url = reverse('obtain_token')
        response = self.client.post(url, data={'username': 'testtest', 'password': 'qwe123456', })

        if response.status_code == 200:
            token = response.json().get('token')
            self.assertIsNotNone(token)

    def test_verify_token(self):

        url = reverse('obtain_token')
        response = self.client.post(url, data={'username': 'testtest', 'password': 'qwe123456', })
        token = response.json().get('token')

        if response.status_code == 200 and token:
            headers = {'Content-type': 'application/json'}
            url = reverse('verify_token')

            response = self.client.post(url, headers=headers, data={'token': token, })

            if response.status_code == 200:
                new_token = response.json().get('token')
                self.assertIsNotNone(new_token)
