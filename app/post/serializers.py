from rest_framework import serializers

from .models import *


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = '__all__'


class PostLikeSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostLike
        fields = '__all__'
