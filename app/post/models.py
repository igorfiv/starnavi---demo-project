from django.db import models
from django.utils import timezone

from app.user.models import CustomUser
from .functions import unique_upload_filename


class Post(models.Model):
    """
    Blog post model
    """
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='User')
    title = models.CharField(max_length=255, blank=False, null=False, verbose_name='Post title')
    text = models.TextField(blank=False, null=False, verbose_name='Post text')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Created at: ')
    edited = models.DateTimeField(default=None, blank=True, null=True, verbose_name='Edited at: ')
    posted = models.DateTimeField(default=timezone.now, blank=True, null=True, verbose_name='Posted at: ')
    active = models.BooleanField(default=True, verbose_name='Is active?')
    image = models.ImageField(null=True, blank=True, upload_to=unique_upload_filename)
    like_count = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.edited = timezone.now()
        super(Post, self).save(*args, **kwargs)

    def like(self):
        self.like_count += 1
        self.save()

    def unlike(self):
        if self.like_count > 0:
            self.like_count -= 1
            self.save()


class PostLike(models.Model):
    """
    Model to store the post's likes information
    """

    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        Post.objects.filter(pk=self.post.id).first().like()
        super(PostLike, self).save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        Post.objects.filter(pk=self.post.id).first().unlike()
        super(PostLike, self).delete()
