import os
import uuid

def unique_upload_filename(instance, filename):
    """
    Generate unique filename and path for the uploaded images
    :param instance:
    :param filename:
    :return: string
    """

    filename, file_extension = os.path.splitext(filename)

    return "objects-photos/{}{}".format(uuid.uuid4(), file_extension)
