from django.contrib import admin

from .models import *


class PostAdmin(admin.ModelAdmin):
    model = Post

    list_display = ('title', 'user', 'created', 'posted', )

    raw_id_fields = ('user', )

    readonly_fields = ('edited', 'like_count', )


admin.site.register(Post, PostAdmin)
