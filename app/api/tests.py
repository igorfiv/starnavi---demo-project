from django.test import TestCase
from rest_framework.test import RequestsClient
from django.urls import reverse


class UserTest(TestCase):

    def setUp(self):

        self.dummy_user_data = {'username': 'testtest',
                                'email': '123456@gmail.com',
                                'password1': 'QweQwe1221',
                                'password2': 'QweQwe1221', }

        url = reverse('rest_register')

        response = self.client.post(url, data=self.dummy_user_data)

        self.user_data = response.json()

        if self.user_data:
            self.headers = {'Authorization': 'JWT {}'.format(self.user_data['token'])}

        url = reverse('posts-list')
        post = {}
        post.update({'title': 'test_title'})
        post.update({'text': 'test_description'})
        post.update({'user': self.user_data['user']['pk']})
        post.update({'active': True})

        response = self.client.post(url, headers=self.headers, data=post)

        self.user_post = response.json()

    def test_register_user(self):
        url = reverse('rest_register')

        response = self.client.post(url, data=self.dummy_user_data)

        if response.status_code == 200:
            token = response.json().get('token')
            self.assertIsNotNone(token)

    def test_user_list(self):
        url = reverse('user-list')

        response = self.client.get(url, headers=self.headers)

        self.assertEqual(response.status_code, 200)

    def test_posts_list(self):
        url = reverse('posts-list')

        response = self.client.get(url, headers=self.headers)

        self.assertEqual(response.status_code, 200)

    def test_post_create(self):
        url = reverse('posts-list')
        post = {}
        post.update({'title': 'test_title'})
        post.update({'text': 'test_description'})
        post.update({'user': self.user_data['user']['pk']})
        post.update({'active': True})

        response = self.client.post(url, headers=self.headers, data=post)

        self.assertEqual(response.status_code, 201)

    def test_like(self):
        url = reverse('like')
        data = {'user': self.user_data['user']['pk'],
                'post': self.user_post.get('id')}

        response = self.client.post(url, headers=self.headers, data=data)

        self.assertEqual(response.status_code, 201)

    def test_unlike(self):
        url = reverse('like')
        data = {'user': self.user_data['user']['pk'],
                'post': self.user_post.get('id')}

        response = self.client.post(url, headers=self.headers, data=data)

        like_data = response.json()

        url = reverse('unlike', kwargs={'pk': like_data.get('id')})
        print(url)
        response = self.client.delete(url, headers=self.headers, data=data)

        self.assertEqual(response.status_code, 204)
