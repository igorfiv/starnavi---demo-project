import datetime
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from django.utils import timezone

from app.user.models import CustomUser
from app.user.serializers import UserSerializer
from app.post.models import Post, PostLike
from app.post.serializers import PostSerializer, PostLikeSerializer


class UserListView(ListCreateAPIView):
    """
    Return users list
    TODO This is not listed in requirements!!!
    """
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer


class PostList(ListCreateAPIView):
    """
    Return the post list
    """

    queryset = Post.objects.filter(active=True)
    serializer_class = PostSerializer


class Like(ListCreateAPIView):
    queryset = PostLike.objects.all()
    serializer_class = PostLikeSerializer


class Unlike(RetrieveUpdateDestroyAPIView):
    queryset = PostLike.objects.all()
    serializer_class = PostLikeSerializer
