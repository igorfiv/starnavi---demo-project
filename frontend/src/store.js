import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  jwt: localStorage.getItem('t'),
};

export default new Vuex.Store({
  strict: true,
  state,
  mutations: {},
  actions: {},
});
