import { HTTP } from "./common";

export const postsApi = {
  postsList() {
    const config = { headers: { 'Authorization': 'JWT ' + localStorage.getItem('t'), }};
    return HTTP.get('posts/', config).then((response) => {
      return response
    }).catch((error) => {
      return error.response.data
    })
  },
  postCreate( data ) {
    const config = { headers: { 'Authorization': 'JWT ' + localStorage.getItem('t'),
                                'Content-Type': 'multipart/form-data', }};
    return HTTP.post('posts/', data, config).then((response) => {
      return response
    }).catch((error) => {
      return error.response.data
    })
  },
  likeList() {
    const config = { headers: { 'Authorization': 'JWT ' + localStorage.getItem('t'), }};
    return HTTP.get('like/', config).then((response) => {
      return response
    }).catch((error) => {
      return error.response.data
    })
  },
  likeAdd( data ) {
    const config = { headers: { 'Authorization': 'JWT ' + localStorage.getItem('t'), }};
    return HTTP.post('like/', data, config).then((response) => {
      return response
    }).catch((error) => {
      return error.response.data
    })
  },
  likeRemove( pk ) {
    const config = { headers: { 'Authorization': 'JWT ' + localStorage.getItem('t'), }};
    return HTTP.delete('unlike/' + pk +'/', config).then((response) => {
      return response
    }).catch((error) => {
      return error.response.data
    })
  }
};
