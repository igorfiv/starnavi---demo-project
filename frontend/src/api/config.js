export const API_URL = 'http://localhost:8000/api/v1/';
export const STATIC_URL = '/static/';
export const MEDIA_URL = '/media/';
export const OBTAIN_JWT_URL = 'api-token-auth/';
export const REFRESH_JWT_URL = 'api-token-refresh/';
export const VERIFY_JWT_URL = 'api-token-verify/'; // TODO Need change domen
