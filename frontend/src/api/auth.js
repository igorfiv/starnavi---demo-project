import { HTTP } from "./common";
import { REFRESH_JWT_URL, VERIFY_JWT_URL } from "./config";
import jwt_decode from "jwt-decode";

export const authApi = {
  signupUser( data ) {
    const config = { headers: { 'Content-Type': 'multipart/form-data', }};
    return HTTP.post('rest-auth/registration/', data, config).then((response) => {
      return response
    }).catch((error) => {
      return error.response.data
    })
  },
  loginUser( data ) {
    const config = { headers: { 'Content-Type': 'multipart/form-data', }};
    return HTTP.post('rest-auth/login/', data, config).then((response) => {
      return response
    }).catch((error) => {
      return error.response.data
    })
  },
  refreshToken(currentToken, iat) {
    return HTTP.post(REFRESH_JWT_URL, {'token' :currentToken, 'orig_iat': iat})
      .then((response) => {
        return response
      })
      .catch((error) => {
        console.log(error)
      });
  },
  verifyToken() {
    let currentToken = localStorage.getItem('t')
    return HTTP.post(VERIFY_JWT_URL, {'token': currentToken})
      .then((response) => {
        console.log(response)
        if (response.status === 200) {
          authApi.refreshToken(response.data.token, localStorage.getItem('orig_iat')).then((newData) => {
            console.log('newData', newData)
            localStorage.setItem('t', newData.data.token);
          })
        }
        return response
      })
      .catch((error) => {
        console.log(error)
      });
  },
};
