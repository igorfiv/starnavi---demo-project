import axios from 'axios';
// import { getCookie } from '@/include/csrf-token';
import { API_URL } from './config'

export const HTTP = axios.create({
  baseURL: API_URL,
});
