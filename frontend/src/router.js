import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from '@/views/Login.vue';
import Registration from '@/views/Registration.vue';
import NewPost from '@/views/NewPost.vue';
import { authApi } from "./api/auth";

Vue.use(Router);

const router =  new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/new-post',
      name: 'new-post',
      component: NewPost,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
});

router.beforeEach((to, from, next) => {
  console.log('working ... ')
  authApi.verifyToken()
  next()
})

export default router;
