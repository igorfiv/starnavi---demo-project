import os
import configparser
import requests
import random

from functions import password_generator, create_user, read_korrespondent_cinema_rss_feed, load_image, get_posts_id

# load config
config = configparser.ConfigParser()
config.read('config.ini')

error = False

try:
    number_of_users = int(config['DEFAULT']['number_of_users'])
except ValueError:
    error = True
    print('Error. number_of_users mast be integer value')

try:
    max_posts_per_user = int(config['DEFAULT']['max_posts_per_user'])
except ValueError:
    error = True
    print('Error. max_posts_per_user mast be integer value')

try:
    max_likes_per_user = int(config['DEFAULT']['max_likes_per_user'])
except ValueError:
    error = True
    print('Error. max_likes_per_user mast be integer value')

base_api_url = config['URLS']['base_api_url']
registration_url = config['URLS']['registration_url']
create_post_url = config['URLS']['create_post_url']
create_post_like = config['URLS']['create_post_like']

if error:
    print('Error in config file!!!')

if os.path.exists('user.txt'):
    os.remove('user.txt')

# load additional data
first_names = requests\
    .get('https://raw.githubusercontent.com/danielmiessler/SecLists/master/Usernames/Names/femalenames-usa-top1000.txt')\
    .content\
    .decode('utf-8')\
    .split()
last_names = requests\
    .get('https://raw.githubusercontent.com/danielmiessler/SecLists/master/Usernames/Names/familynames-usa-top1000.txt')\
    .content\
    .decode('utf-8')\
    .split()

post_dummy_data = read_korrespondent_cinema_rss_feed()

if not error:

    users_count = 1

    while number_of_users >= users_count:
        user_data = create_user(first_names, last_names, url=base_api_url + registration_url)

        if user_data:
            post_count = 1

            while max_posts_per_user >= post_count:
                rnd = random.randint(1, len(post_dummy_data)-1)
                headers = {'Authorization': 'JWT {}'.format(user_data['token'])}
                post = post_dummy_data[rnd]
                post.update({'user': user_data['user']['pk']})
                post.update({'active': True})
                files = {}
                if post.get('image'):
                    files = load_image(post.get('image'))

                new_post = requests.post(base_api_url + create_post_url, headers=headers, data=post, files=files)

                if new_post.status_code == 201:
                    print('Posts per user count', post_count)
                    post_count += 1

            list_post_id = get_posts_id(base_api_url + create_post_url, user_data['token'])

            if list_post_id:

                like_count = 0

                if len(list_post_id) < max_likes_per_user:
                    max_likes_per_user = len(list_post_id)

                while max_likes_per_user >= like_count:

                        index = random.randint(1, len(list_post_id)-1)
                        data = {'user': user_data['user']['pk'],
                                'post': list_post_id[index]}
                        new_like = requests.post(base_api_url + create_post_like, headers=headers, data=data)

                        if new_like.status_code == 201:
                            print('Likes count', like_count)
                            like_count += 1

            print('Users count', users_count)
            users_count += 1

if os.path.exists('temp.jpg'):
    os.remove('temp.jpg')
