"""
Functions for the posting BOT
"""

import re
import random
import requests
import shutil
import string
from xml.etree import ElementTree


def password_generator(size=8, chars=string.ascii_letters + string.digits):
    """
    Returns a string of random characters, useful in generating temporary
    passwords for automated password resets.

    size: default=8; override to provide smaller/larger passwords
    chars: default=A-Za-z0-9; override to provide more/less diversity

    Credit: Ignacio Vasquez-Abrams
    Source: http://stackoverflow.com/a/2257449
    """
    return ''.join(random.choice(chars) for i in range(size))


def load_image(url):
    """
    Load image from url
    :param url: url to the image
    :return: image object
    """

    files = {}

    loaded_image = requests.get(url, stream=True)
    if loaded_image.status_code == 200:
        with open('temp.jpg', 'wb') as f:
            loaded_image.raw.decode_content = True
            shutil.copyfileobj(loaded_image.raw, f)

        files = {'image': open('temp.jpg', 'rb')}

    return files


def read_korrespondent_cinema_rss_feed():
    """
    Read the RSS feed of korrespondent.net
    Used for dummy post data
    :return: dictionary with the post data
    """

    news = requests.get('http://k.img.com.ua/rss/ru/cinema.xml', stream=True)
    news.raw.decode_content = True
    events = ElementTree.iterparse(news.raw)
    post = {}
    result = []
    count = 1

    for event, elem in events:
        if elem.tag == 'item':

            post.update({'title': elem[0].text})
            post.update({'text': elem[3].text})

            matches = re.findall('src="([^"]+)"', elem[2].text)
            if matches:
                post.update({'image': matches[0]})

            if post.__len__() >= 2:
                result.append(dict(post))
                count += 1
                post.clear()

    return result


def create_user(first_names, last_names, url):
    """
    Generate a user based on first_name and last_name from the lists
    :param first_names: list of values
    :param last_names: list of values
    :param url: API endpoint to create a user
    :return: user data or False
    """

    if first_names and last_names:
        rnd = random.randint(1, 1000)
        password = password_generator()
        first_name = first_names[rnd]
        last_name = last_names[rnd]

        data = {'username': '{}_{}'.format(first_name, last_name),
                'email': '{}_{}{}'.format(first_name, last_name, '@gmail.com'),
                'password1': password,
                'password2': password, }

        user = requests.post(url, data=data)

        if user.status_code == 201:
            report_file = open('user.txt', 'a')
            report_file.write('{}:{}\n'.format(data.get('username'), password))
            report_file.close()
            return user.json()

    return False


def get_posts_id(url, user_token):
    """
    Return the list of the post's ids
    :param url: API url to get list of the posts
    :param user_token: user token
    :return: list with post id
    """
    headers = {'Authorization': 'JWT {}'.format(user_token)}

    all_post = requests.get(url, headers=headers)
    list_post_id = []

    if all_post.status_code == 200:
        for i in all_post.json():
            list_post_id.append(i.get('id'))

    return list_post_id
